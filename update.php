<?php
include "inc/header.php";
include "config.php";
include "Database.php";
?>
<?php
$id = $_GET['id'];
$db = new Database();
$query = "SELECT * FROM tbl_user WHERE id=$id";
$getData  = $db->select($query)->fetch_assoc();

if (isset($_POST['submit'])){
    $name = mysqli_escape_string($db->link,$_POST['name']);
    $email = mysqli_escape_string($db->link,$_POST['email']);
    $skill = mysqli_escape_string($db->link,$_POST['skill']);

    if ($name == "" || $email == "" || $skill == ""){
        $error = "<span class='alert alert-danger'>Field Must Not Be Empty!!</span>";
    }else{
        $query = "UPDATE tbl_user
          SET
          name  = '$name',
          email = '$email',
          skill = '$skill'
          WHERE id = $id";

        $update = $db->update($query);
    }
}
?>
<div class="text-center">
    <?php
    if (isset($_POST['delete'])){
        $query = "DELETE FROM tbl_user WHERE id =$id";
        $deleteData = $db->delete($query);
    }

    ?>
    <?php
    if (isset($error)){
        echo "<span class='alert alert-danger'>$error</span>";
    }
    ?>
</div>
<div class="py-3" style="width: 60%;margin: 0 auto">
    <form action="update.php?id=<?php echo $id;?>" method="post">
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Name:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="name" id="name" value="<?php echo $getData['name']?>">
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label">Email:</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" name="email" id="email" value="<?php echo $getData['email']?>">
            </div>
        </div>
        <div class="form-group row">
            <label for="skill" class="col-sm-2 col-form-label">Skill:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="skill" id="skill" value="<?php echo $getData['skill']?>">
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 col-form-label"></label>
            <div class="col-sm-10">
                <input type="submit" name="submit" value="Update">
                <input type="reset"  value="Reset">
                <input type="submit" name="delete" value="Delete">
            </div>
        </div>
    </form>
</div>

<?php include "inc/footer.php";?>
