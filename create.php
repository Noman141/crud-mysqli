<?php
include "inc/header.php";
include "config.php";
include "Database.php";
?>
<?php
$db = new Database();

if (isset($_POST['submit'])){
    $name = mysqli_escape_string($db->link,$_POST['name']);
    $email = mysqli_escape_string($db->link,$_POST['email']);
    $skill = mysqli_escape_string($db->link,$_POST['skill']);

    if ($name == "" || $email == "" || $skill == ""){
        $error = "<span class='alert alert-danger'>Field Must Not Be Empty!!</span>";
    }else{
        $query = "INSERT INTO tbl_user(name,email,skill) VALUES ('$name','$email','$skill')";
        $create = $db->insert($query);
    }
}
?>
<div class="text-center">
    <?php
    if (isset($error)){
        echo "<span class='alert alert-danger'>$error</span>";
    }
    ?>
</div>
<div class="py-3" style="width: 60%;margin: 0 auto">
    <form action="" method="post">
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Name:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name">
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label">Email:</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" name="email" id="email" placeholder="Enter Email">
            </div>
        </div>
        <div class="form-group row">
            <label for="skill" class="col-sm-2 col-form-label">Skill:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="skill" id="skill" placeholder="Enter Skill">
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 col-form-label"></label>
            <div class="col-sm-10">
                <input type="submit" name="submit" value="Submit">
                <input type="reset"  value="Reset">
            </div>
        </div>
    </form>
</div>

<?php include "inc/footer.php";?>
