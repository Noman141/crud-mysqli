<?php
include "inc/header.php";
include "config.php";
include "Database.php";
?>

<?php
$db = new Database();

$query = "SELECT * FROM tbl_user";
$read  = $db->select($query);

?>
<div class="text-center">
    <?php
    if(isset($_GET['msg'])){
        echo "<span class='alert alert-success'>".$_GET['msg']."</span>";
    }
    ?>
</div>
<div class="py-3">
    <table class="table table-striped table-dark m-0">
        <thead>
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Skill</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 0;
        if ($read){;?>
            <?php while ($row = $read->fetch_assoc()){
                $i++;
             ?>
        <tr>
            <th scope="row"><?php echo $i;?></th>
            <td><?php echo $row['name'];?></td>
            <td><?php echo $row['email'];?></td>
            <td><?php echo $row['skill'];?></td>
            <td>
                <a href="update.php?id=<?php echo urldecode($row['id']);?>">Edit</a>
            </td>
        </tr>
        <?php }?>
        <?php }else {?>
            <p>Data Not Available!!</p>
        <?php }?>
        </tbody>
    </table>
</div>



<?php include "inc/footer.php";?>
